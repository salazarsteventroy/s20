// JSON OBJECTS
// JSON stands for JavaScript Object Notation

/*
	json data format
	syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"

		}
*/


/*	JS Object
	{
		city: "QC",
		province: "Metro Manila",
		country: 'Philippines'
	}


JSON
{
	"city": "QC",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON ARRAY
/*"cities": [
	{
	"city": "QC",
	"province": "Metro Manila",
	"country": "Philippines"
	},
	{
	"city": "QC",
	"province": "Metro Manila",
	"country": "Philippines"
	}
]
*/

// JSON METHODS
// the json object contains methods for parsing and converting data into a stringified json

/*stringified JSON is a java script
object convrted into a string to be used in 
other function of a javascript application*/

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'batch y'
	}
]

console.log('result from stringify method')

/*The stringify method is used to convert JS objects into JSON(string)*/
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'manila',
		country: 'philippines'
	}
})

console.log(data)

let data1 = JSON.stringify({
	name: (prompt()),
	age: (prompt()),
	address: (prompt())
})

console.log(data1)
/*let number = Number(prompt("provibe a number"));

console.log(number)
*/

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
]`

console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `
    	{
    		"name": "John",
    		"age" : "31",
    		"address" : {
    			"city": "Manila",
    			"country" : "Philippines"
    		}
    	}
    `

    console.log(JSON.parse(stringifiedObject))

    // client to server (stringify)
    // server to client (parse)